# Playground for Selenium and OWASP Zap

A local OWASP Zap with local settings may not provide desired results, so in most cases
a remote Zap/selenium server will provide significant better reports.

# Requirements

You need a dockerized Selenium and Zap, e.g. https://gitlab.com/ip6li/docker-selenium

# Settings

Customize settings in class TestLib

# References

https://github.com/zaproxy/zap-api-java

https://www.javadoc.io/doc/org.zaproxy/zap-clientapi/latest/index.html

https://medium.com/@kshitishirke/automate-zap-security-tests-with-selenium-webdriver-8f72a8811524


