package test.selenium;

public final class TestLib {
    public static final String TARGET = "https://example.com";
    public static final String Node = "http://localhost:4444";
    public static final String zapHost = "127.0.0.1";
    public static final int zapPort = 8086;
    public static final String ZAP_API_KEY = null; // Change this if you have set the apikey in ZAP via Options / API

    public static final String xmlFileName = "report.xml";
    public static final String htmlFileName = "report.html";
    public static final String detailsFileName = "details.txt";
}
