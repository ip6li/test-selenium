package test.selenium;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertTrue;


class TestSelenium {
    private static final Logger logger = LoggerFactory.getLogger(TestSelenium.class);


    static RemoteWebDriver initSeleniumHub() throws MalformedURLException {
        final String PROXY = "zap:8086";
        final Proxy proxy = new Proxy();
        proxy.setHttpProxy(PROXY)
                .setFtpProxy(PROXY)
                .setSslProxy(PROXY);

        final DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        caps.setAcceptInsecureCerts(true);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.PROXY, proxy);

        /* The execution happens on the Selenium Grid with the address mentioned earlier */
        return new RemoteWebDriver(new URL(TestLib.Node), caps);
    }

    @Test
    void testDavw() throws MalformedURLException {
        final RemoteWebDriver driver;
        driver = initSeleniumHub();
        driver.navigate().to(TestLib.TARGET);
        WebElement usernameTxt = driver.findElement(By.name("username"));
        WebElement passwordTxt = driver.findElement(By.name("password"));
        WebElement btnLogin = driver.findElement(By.name("Login"));
        usernameTxt.sendKeys("admin");
        passwordTxt.sendKeys("password");
        btnLogin.click();

        boolean titleFound = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.titleIs("Welcome :: Damn Vulnerable Web Application (DVWA) v1.10 *Development*"));
        assertTrue(titleFound);
        final String pageSource = driver.getPageSource();
        logger.info(pageSource);
        driver.quit();
    }
}
