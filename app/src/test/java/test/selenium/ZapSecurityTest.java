package test.selenium;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ClientApi;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

class ZapSecurityTest {
    private static final Logger logger = LoggerFactory.getLogger(ZapSecurityTest.class);
    private static final int percent100 = 100;
    private static final long wait1s = 1000;
    private static final long wait2s = 2 * wait1s;
    private static final long wait5s = 5 * wait1s;
    private final ClientApi api = new ClientApi(TestLib.zapHost, TestLib.zapPort, TestLib.ZAP_API_KEY);
    private String scanid;

    private void saveToFile(String fileName, byte[] content)
            throws IOException {
        try (final FileOutputStream fileOutputStream = new FileOutputStream(fileName)) {
            fileOutputStream.write(content);
        }
    }

    private void spider() {
        try {
            // Start spidering the target
            logger.info(String.format("Spider : %s", TestLib.TARGET));
            // It's not necessary to pass the ZAP API key again, already set when creating the
            // ClientApi.
            final ApiResponse resp = api.spider.scan(TestLib.TARGET, null, null, null, null);
            int progress;

            // The scan now returns a scan id to support concurrent scanning
            scanid = ((ApiResponseElement) resp).getValue();

            // Poll the status until it completes
            do {
                Thread.sleep(wait1s);
                progress =
                        Integer.parseInt(
                                ((ApiResponseElement) api.spider.status(scanid)).getValue());
                logger.info(String.format("Spider progress : %s%%", progress));
            } while (progress < percent100);
            logger.info("Spider complete");

            // Give the passive scanner a chance to complete
            Thread.sleep(wait2s);
        } catch (Exception e) {
            logger.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void activeScan() {
        int progress;
        try {
            logger.info(String.format("Active scan : %s", TestLib.TARGET));
            final ApiResponse resp = api.ascan.scan(
                    TestLib.TARGET,
                    "True",
                    "False",
                    null,
                    null,
                    null
            );

            // The scan now returns a scan id to support concurrent scanning
            scanid = ((ApiResponseElement) resp).getValue();

            // Poll the status until it completes
            do {
                Thread.sleep(wait5s);
                progress =
                        Integer.parseInt(
                                ((ApiResponseElement) api.ascan.status(scanid)).getValue());
                logger.info(String.format(
                        "Active Scan progress [%s]: %s%%",
                        new Date().toString(),
                        progress
                        )
                );
            } while (progress < percent100);
            logger.info("Active Scan complete");

            logger.info("Alerts:");
            saveToFile(TestLib.xmlFileName, api.core.xmlreport());
            saveToFile(TestLib.htmlFileName, api.core.htmlreport());
            ApiResponse numberOfMessages = api.core.numberOfMessages(TestLib.TARGET);
            ApiResponse messages = api.core.messages(TestLib.TARGET, "0", numberOfMessages.toString());
            saveToFile(TestLib.detailsFileName, messages.toString().getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            logger.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    void testZap() {
        spider();
        activeScan();
    }
}
