#!/usr/bin/env bash

get_alerts() {
  ALERTS_URL="http://$1:$2/"
  ALERTS_URL+="JSON/core/view/alerts/?"
  ALERTS_URL+="zapapiformat=JSON&"
  ALERTS_URL+="formMethod=GET&"
  ALERTS_URL+="baseurl=https://$3&"
  curl -s $ALERTS_URL > alerts.json
}

get_report() {
  REPORT_URL="http://$1:$2/"
  REPORT_URL+="OTHER/core/other/htmlreport/?"
  REPORT_URL+="formMethod=GET"
  curl -s $REPORT_URL > report.html
}

. .env

get_alerts $ZAP_HOST $ZAP_PORT $TARGET
get_report $ZAP_HOST $ZAP_PORT
