#!/usr/bin/env bash

start_active_scan() {
  SCAN_URL="http://$1:$2/"
  SCAN_URL+="JSON/ascan/action/scan/?"
  SCAN_URL+="zapapiformat=JSON&"
  SCAN_URL+="formMethod=GET&"
  SCAN_URL+="url=$3&"

  # Start Active ZAP Scan
  SCAN_ID_RES=$(curl -s $SCAN_URL)
  # Parse for scan ID
  SCAN_ID=$(echo $SCAN_ID_RES | jq -r '.scan')
  # Display scan ID
  echo Scan ID: $SCAN_ID
}

. .env
TARGET="https://demo-dast.felsing.net"
start_active_scan $ZAP_HOST $ZAP_PORT $TARGET
