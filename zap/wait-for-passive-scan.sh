#!/usr/bin/env bash

wait_for_passive_scan_to_complete() {
  STATUS_URL="http://$1:$2/"
  STATUS_URL+="JSON/pscan/view/recordsToScan/?"
  STATUS_URL+="zapapiformat=JSON&"
  STATUS_URL+="formMethod=GET&"
  SCAN_STATUS=0
  until [ $SCAN_STATUS -eq 0 ]; do
    sleep10
    # Get Scan status
    SCAN_STATUS_RES=$(curl -s $STATUS_URL)
    # Parse scan status
    SCAN_STATUS=$(echo $SCAN_STATUS_RES | jq -r '.recordsToScan')
    # Display status
    echo Scan $SCAN_STATUS% complete
  done
}

. .env

wait_for_passive_scan_to_complete $ZAP_HOST $ZAP_PORT
