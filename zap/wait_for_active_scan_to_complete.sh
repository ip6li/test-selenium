#!/usr/bin/env bash

wait_for_active_scan_to_complete() {
  STATUS_URL="http://$1:$2/"
  STATUS_URL+="JSON/ascan/view/status/?"
  STATUS_URL+="zapapiformat=JSON&"
  STATUS_URL+="apikey=&"
  STATUS_URL+="formMethod=GET&"
  STATUS_URL+="scanId=$SCAN_ID"

  SCAN_STATUS=0
  until [ $SCAN_STATUS -eq 100 ]; do
    sleep 10
    # Get Scan status
    SCAN_STATUS_RES=$(curl -s $STATUS_URL)
    # Parse scan status
    SCAN_STATUS=$(echo $SCAN_STATUS_RES | jq -r '.status')
    # Display status
    echo Scan $SCAN_STATUS% complete
  done
  echo Active Scan Complete
}

. .env

wait_for_active_scan_to_complete $ZAP_HOST $ZAP_PORT
